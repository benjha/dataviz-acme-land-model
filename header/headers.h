
#ifndef __headers_h__
#define __headers_h__

#include <vtkScatterPlotMatrix.h>
#include <vtkRenderWindow.h>
#include <vtkChart.h>
#include <vtkPlot.h>
#include <vtkTable.h>
#include <vtkContextView.h>
#include <vtkContextScene.h>
#include <vtkAnnotationLink.h>
#include <vector>
#include <pthread.h>
#include <time.h>
#include <vtkSmartPointer.h>
#include <vtkChartXY.h>
#include <vtkAxis.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkDelimitedTextReader.h>
#include <vtksys/SystemTools.hxx>
#include <vtkCallbackCommand.h>
#include <vtkFloatArray.h>
#include <vtkStringArray.h>
#include <vtkDoubleArray.h>
#include <vtkIntArray.h>
#include <unistd.h>
#include <vtkEventQtSlotConnect.h>
#include <vtkPolygon.h>
#include <vtkPoints.h>
#include <vtkContextTransform.h>
#include <vtkHeatmapItem.h>


//#define WIN_WIDTH 	1900
//#define WIN_HEIGHT 	1000
#define GAP			40

#define NUM_CHARTS	7
#define DATA_SIZE   1000

#define VTK_CREATE(type, name) \
  vtkSmartPointer<type> name = vtkSmartPointer<type>::New();


#endif
