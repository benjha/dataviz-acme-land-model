#define vtkRenderingCore_AUTOINIT 3(vtkInteractionStyle, vtkRenderingFreeType, vtkRenderingOpenGL2)
#define vtkRenderingContext2D_AUTOINIT 1(vtkRenderingContextOpenGL2)

#include <cMainWindow.h>
#include <ui.h>

cMainWindow *mainWin;

int main( int argc, char *argv[] )
{
	QApplication 	app (argc, argv);

	mainWin = new cMainWindow (argc, argv);

	mainWin->show ();
	return app.exec();
}
