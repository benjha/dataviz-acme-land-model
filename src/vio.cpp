#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <sys/wait.h>
#include <inttypes.h>
#include <assert.h>
#include <sys/time.h>

#include <vio.h>



uint32_t plen = 0;
cci_endpoint_t *ep = NULL;
char *ep_uri = NULL;
cci_connection_t *conn = NULL;
cci_rma_handle_t *local = NULL;


int vio_init(cci_endpoint_t **endpointp, char **urip)
{
	int ret = 0;
	uint32_t caps = 0;

	ret = cci_init(CCI_ABI_VERSION, 0, &caps);
	if (ret) {
		fprintf(stderr, "%s: cci_init() failed with %s\n",
				__func__, cci_strerror(NULL, (cci_status) ret));
		goto out;
	}

	ret = cci_create_endpoint(NULL, 0, &ep, NULL);
	if (ret) {
		fprintf(stderr, "%s: cci_create_endpoint() failed with %s\n",
				__func__, cci_strerror(NULL, (cci_status) ret));
		goto out;
	}

	ret = cci_get_opt(ep, CCI_OPT_ENDPT_URI, &ep_uri);
	if (ret) {
		fprintf(stderr, "%s: cci_get_opt() failed with %s\n",
				__func__, cci_strerror(ep, (cci_status) ret));
		goto out;
	}

	if (endpointp)
		*endpointp = ep;
	if (urip)
		*urip = ep_uri;

    out:
	if (ret)
		vio_finalize();
	return ret;
}

int vio_register(void *buffer, uint32_t len, cci_rma_handle_t **handlep)
{
	int ret = 0;
	cci_rma_handle_t *handle = NULL;

	plen = len;

	ret = cci_rma_register(ep, buffer, (uint64_t)len,
		CCI_FLAG_WRITE|CCI_FLAG_READ, &handle);
	if (ret) {
		fprintf(stderr, "%s: cci_rma_register() failed with %s\n",
				__func__, cci_strerror(ep, (cci_status) ret));
	}

	if (handlep)
		*handlep = handle;
	else
		local = handle;

	return ret;
}

int vio_connect(char *server_uri)
{
	int ret = 0, len = 0, ready = 0;
	char data[256];

	// original:
	// vio_msg_t *msg = (void*) data;
	//changed to:
	vio_msg_t *msg = (vio_msg_t*) data;

	len = strlen(ep_uri);

	memset(data, 0, sizeof(data));

	msg->connect.type = CONNECT;
	msg->connect.uri_len = len;
	msg->connect.buffer_len = plen;
	memcpy((void*) &msg->connect.handle, local, sizeof(*local));
	memcpy(&msg->connect.uri, ep_uri, len);

	len += sizeof(*msg);

	ret = cci_connect(ep, server_uri, msg, len, CCI_CONN_ATTR_RO,
			NULL, 0, NULL);
	if (ret) {
		fprintf(stderr, "%s: cci_connect() failed with %s\n",
				__func__, cci_strerror(ep, (cci_status) ret));
		goto out;
	}

	do {
		cci_event_t *event = NULL;

		ret = cci_get_event(ep, &event);
		if (!ret) {
			switch (event->type) {
			case CCI_EVENT_CONNECT:
				conn = event->connect.connection;
				ready++;
				break;
			default:
				fprintf(stderr, "%s: ignoring %s\n", __func__,
						cci_event_type_str(event->type));
				break;
			}
			cci_return_event(event);
		}
	} while (!ready);

	if (!conn) {
		ret = ENOTCONN;
		fprintf(stderr, "%s: CCI connect failed\n", __func__);
	}

    out:
	return ret;
}

int vio_disconnect(void)
{
	int ret = 0;
	vio_msg_t *msg = new vio_msg_t();

	if (!conn)
		return ENOTCONN;

	msg->bye.type = BYE;

	ret = cci_send(conn, msg, sizeof(msg->bye),
			(void*)(uintptr_t)0xdeadbeef, 0);
	if (!ret) {
		int done = 0, i = 0;
		cci_event_t *event = NULL;

		do {
			ret = cci_get_event(ep, &event);
			if (!ret) {
				const vio_msg_t *rx = NULL;

				switch (event->type) {
				case CCI_EVENT_SEND:
					assert(event->send.context ==
							(void*)(uintptr_t)0xdeadbeef);
					break;
				case CCI_EVENT_RECV:
					// original:
					//rx = event->recv.ptr;
					//changed to:
					rx = (vio_msg_t *)event->recv.ptr;
					assert(rx->fini.type == FINISHED);
					break;
				default:
					break;
				}
				cci_return_event(event);
				done++;
			}
		} while (done < 2);

		/* to allow CCI to ack the send? */
		for (i = 0; i < 10; i++)
			cci_get_event(ep, &event);

	}

	ret = cci_disconnect(conn);
	if (ret) {
		fprintf(stderr, "%s: cci_disconnect() failed with %s\n",
				__func__, cci_strerror(ep, (cci_status) ret));
	}

	delete msg;
	return ret;
}

int vio_write(uint32_t len)
{
	int ret = 0, done = 0;
	static int i = 0;
	vio_msg_t *msg = new vio_msg_t();

	i++;

	msg->request.type = WRITE_REQ;
	msg->request.cookie = i;
	msg->request.len = len;

	ret = cci_send(conn, msg, sizeof(msg->request), (void*)(uintptr_t)i, 0);
	if (ret) {
		fprintf(stderr, "%s: cci_send() failed with %s\n",
				__func__, cci_strerror(ep, (cci_status) ret));
		goto out;
	}

	do {
		cci_event_t *event = NULL;

		ret = cci_get_event(ep, &event);
		if (!ret) {
			const vio_msg_t *rx = 0;

			switch (event->type) {
			case CCI_EVENT_SEND:
				assert(event->send.context == (void*)(uintptr_t)i);
				break;
			case CCI_EVENT_RECV:
				// Original:
				//rx = event->recv.ptr;
				// changed to:
				rx = (vio_msg_t *)event->recv.ptr;
				assert(rx->done.type == WRITE_DONE);
				assert(rx->done.cookie == i);
				break;
			default:
				fprintf(stderr, "%s: ignoring %s\n",
					__func__, cci_event_type_str(event->type));
				break;
			}
			cci_return_event(event);
			done++;
		}
	} while (done < 2);

    out:
    delete msg;
	return ret;
}

int vio_finalize(void)
{
	int ret = 0;

	if (local) {
		ret = cci_rma_deregister(ep, local);
		if (ret) {
			fprintf(stderr, "%s: cci_rma_deregister() failed with %s\n",
					__func__, cci_strerror(ep, (cci_status) ret));
		}
	}

	if (ep) {
		ret = cci_destroy_endpoint(ep);
		if (ret) {
			fprintf(stderr, "%s: cci_destroy_endpoint() failed with %s\n",
					__func__, cci_strerror(NULL, (cci_status) ret));
		}
	}

	ret = cci_finalize();
	if (ret) {
		fprintf(stderr, "%s: cci_destroy_endpoint() failed with %s\n", __func__,
				cci_strerror(NULL, (cci_status) ret));
	}

	return ret;
}
