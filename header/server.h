
#ifndef SIMULATOR_SERVER_H
#define SIMULATOR_SERVER_H

#include <atomic>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <new>
#include <zlib.h>
#include <cstring>

#include <comm_process.h>
#include <cci.h>

#include <vector>
#include <vtkTable.h>
#include <vtkSmartPointer.h>

#define VIZ_TX_FINI ((void*)((uintptr_t)0x1))
#define CCI_ERR(e){printf("CCI_ERROR %i \n",e); throw e;}

#define	MYUNPACK

typedef struct peer
{
	cci_connection_t	*conn;	/* CCI connection */
	void				*buffer;
	cci_rma_handle_t	*local;	/* Our CCI RMA handle */
	cci_rma_handle_t	*remote; /* Their CCI RMA handle */
	char				*uri;	/* Peer's CCI URI */
	uint32_t			len;	/* RMA buffer length */
	int					done;	/* client sent BYE message */
} peer_t;

typedef struct vio_req
{
	peer_t			*peer;	/* Client peer */
	uint32_t		len;	/* Requesting write of len bytes */
} vio_req_t;

static void free_peer(peer_t *p)
{
	if (!p)
		return;

	free((void*)p->remote);
	free(p->buffer);
	free(p);
}

class cServer{

public:
								cServer		( );
								~cServer	( );

		void 					StopComms 	( );
		void 					StopProcess ( );
		void					myUnpack	(void *buffer, size_t len, vtkSmartPointer<vtkTable> *, vtkSmartPointer<vtkTable> *);
		void					myUnpack 	( void *buffer, size_t len );
		const char* 			URI 		( );
		const cci_endpoint_t* 	EndPT 		( );
		CommProcess 			InitServer	( );

protected:


private:
		void 			HandleConnectRequest	(cci_event_t* event);
		void 			HandleAccept			(cci_event_t* event);
		void 			HandleReceived			(cci_event_t* event);
		void 			HandleWriteReq			(cci_event_t* event);
		void 			HandleSend				(cci_event_t* event,cci_connection_t** conn);
		void 			HandleRMA				(cci_event_t* event);
		void 			HandleFini				(cci_event_t* event,cci_connection_t** conn);
		void 			HandleBye				(cci_event_t* event);
		void 			Comms();
		void			ptrTovtkTable 			(void* in, vtkSmartPointer<vtkTable> out, std::string fieldName,
													int dataTypeSize, int sizeX, int sizeY);
		virtual void 	Process					(	);
		void			printVarOfInterest 		(char *name, vtkSmartPointer<vtkTable> table );
		void			insertStepInSimTable 	(	);
		bool			isVarOfInterest 		(char* name);
		bool			is2DArray 				(const char *name);


		int 						mNumPeers;
		char* 						mpURI;
		std::queue<peer_t> 			peers;
		std::queue<vio_req_t*> 		mReqs;
		std::mutex 					mReqLock;
		std::condition_variable 	mReqCon;
		cci_endpoint_t* 			mpEndPt;
		std::atomic<bool> 			mCommsRunning;
		std::atomic<bool> 			mProcRunning;

};
			
#endif //SIMULATOR_SERVER_H
