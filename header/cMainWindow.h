
#ifndef __MAIN_WINDOW_H__
#define __MAIN_WINDOW_H__

#include <QMainWindow>
#include <string>
#include <vector>
#include <ui.h>
#include <headers.h>
#include <server.h>
#include <sstream>

using namespace std;
//QT_BEGIN_NAMESPACE

typedef struct sChartAxis
{
	std::stringstream xColumn;
	std::vector<std::string> yColumn;
	int step0;
	int step1;
	int pft;
	unsigned char colorScheme;
};


class cMainWindow : public QMainWindow
{
Q_OBJECT

	// Type format files it can open.
	enum {
			CSV,			// txt
			MAPPED_BUFFER,	// binary
		};
public:
						cMainWindow			( int argc, char *argv[] );
						~cMainWindow		( 	) ;
		// stores sim data of 1D variables
		std::vector <vtkSmartPointer<vtkTable>> simData1D;
		// stores sim data of 2D variables


public slots:
		// function call when quitting
		virtual void 	slotExit			( 	);
		// function call when clicking on the list of variables
		void 			updateXaxis 		( QListWidgetItem * ); // empty function
		// function call when clicking on the list of variables
		void 			updateYaxis 		( QListWidgetItem * );
		// function call when clicking on the list of variables
		void 			updateYaxis2DVars	( QListWidgetItem * );
		// function call when selecting an item from PFT combo box
		void			PFTSelection		( int );
		// function call when selecting an item from PFT combo box
		void			PFT2DSelection		( int );
		// function call when spin box clicked (step0)
		void			setStep0			( int );
		void			setStep02DVars			( int );
		// function call when spin box clicked (step1)
		void			setStep1			( int );
		void			setStep12DVars		( int );
		// function call when clicking on the list of variables for time series
		void 			updateTimeSeries	( QListWidgetItem * );
		// refresh  all charts once a time step had been received
		void			updateCharts 		( 	);
		// refresh  all charts once a time step is set by the slider
		void			updateCharts 		( int step	);
		// update selected plot when Clear button is clicked
		void 			resetPlot			( bool  );
		void 			resetPlot2DVars			( bool  );
		// update selected 1D-plot when Ok button is clicked
		void 			ok1DVar				( bool  );
		void 			ok2DVar				( bool  );

private:
		// loads the data file, type format
		void			loadDatafile		(string filename, int type);
		// parses a CSV file and save the info in a vtkTable as a sim. step
		void parseCSV	(string filename);
		// parses a mapped buffer file at save the info in a vtkTable as a sim. step
		void 			parseBuffer	(string filename);
		// generates the plots
		void			initCharts					(	);
		// generates a heatmap
		void			initHeatMaps				(	);
		// generates some cosine, sine data when there's no data file
		void			genSomeData 				(	);
		// fills the list widget with variables names from csv file
		void			populateListWidget			(	);
		// fills the list widget with variables names from simulation data
		void			populateListWidgetFromSim	(	);
		// calculate the time series from simData vector
		void			updateTimeSeries 			( int column );
		void			updateLoop();

		void			query (std::vector<std::string> colName, int step0, int step1, vtkSmartPointer<vtkTable> *out);

		// This comes from QT Designer, it contains the definition of UI widgets
		Ui_MainWindow	*ui;

		//
		int 			getChartClicked (int x, int y);
		virtual bool	eventFilter(QObject *obj, QEvent *event);

		// a vector containing chart instances
		std::vector<vtkChartXY*> 				charts;
		// a vector containing chart instances
		std::vector<vtkHeatmapItem*> 			heatMaps;

		// a vector of heatmap containers
		std::vector<vtkContextTransform*>		heatMapContainer;

		// different support data structures for plotting
		//vtkSmartPointer<vtkTable>				table;
		vtkSmartPointer<vtkTable>				serie;

		vtkSmartPointer<vtkTable>				scratchTable;


		// this links interaction between plots
		vtkSmartPointer<vtkAnnotationLink> 		alink;
		// OpenGL helper
		vtkSmartPointer<vtkContextView> 		view;
		// OpenGL helper
		vtkSmartPointer<vtkContextView> 		view2;


		sChartAxis								chartAxisLabel[NUM_CHARTS];


		// Instance of a CCI Server
		cServer			*cciServer;
		// Threads to control Communications and Data processing
		CommProcess 	commDataProc;

		int chartId;
		// width and height of the vtk window
		int w, h;
		// time step counter;
		int timeStep;
		//
		int currentTimeStep;

		std::string			lastTimeSeriesItem;
		std::stringstream 	uriInfo;

		pthread_t 		DataThread;
		pthread_mutex_t dataMutex;

};


//QT_END_NAMESPACE
#endif //__MAIN_WINDOW_H__
