
#include <iostream>
#include <sstream>
#include <string>
#include <server.h>
#include <vio.h>

#include <vtkTransposeTable.h>
#include <vtkVariantArray.h>
#include <vtkDoubleArray.h>
#include <vtkFloatArray.h>
#include <vtkIntArray.h>

#include <mutex>
#include <condition_variable>

// step info. scope global
extern vtkSmartPointer<vtkTable> step1D;

extern vtkSmartPointer<vtkTable> simData1DTable;
// TODO: remove simData2DTable
extern vtkSmartPointer<vtkTable> simData2DTable;
extern std::vector <vtkSmartPointer<vtkTable>> simData2D;

extern bool  timeStepIsReady;
extern std::mutex m;
extern std::condition_variable cv;
extern unsigned int numTimeSteps;

cServer::cServer() : mpURI(0), mpEndPt(0),
	mCommsRunning(false), peers(), mReqs(), mNumPeers(0), mReqLock(),
	mReqCon(), mProcRunning(false){

}
//
//=======================================================================================
//
cServer::~cServer()
{
	
}
//
//=======================================================================================
//
const char* cServer::URI()
{
	return this->mpURI;
}	
//
//=======================================================================================
//
const cci_endpoint_t* cServer::EndPT()
{
	return this->mpEndPt;
}
//
//=======================================================================================
//
void cServer::StopComms()
{
	this->mCommsRunning = false;
}
//
//=======================================================================================
//
void cServer::HandleConnectRequest(cci_event_t* event)
{
	int ret = 0;
	vio_msg_t* msg = (vio_msg_t*) event->request.data_ptr;
	peer_t* p = NULL;
	long pg_size = 0;

	//Received less than expected.	
	if(event->request.data_len < sizeof(msg->connect)){
		//Log an error.
		CCI_ERR(EINVAL);
	}	

	p = (peer_t*)calloc(1,sizeof(*p));
	if(!p){
		throw std::bad_alloc();
	}

	p->remote = (cci_rma_handle_t*)calloc(1,sizeof(*p->remote));
	if(!p->remote){
		throw std::bad_alloc();
	}

	std::memcpy((void*)p->remote,&msg->connect.handle,sizeof(*p->remote));
	
	//Copy the uri to the peer.
	p->uri = (char*)calloc(1,msg->connect.uri_len+1);
	if(!p->uri){
		throw std::bad_alloc();
	}

	//Copy hte uri to the peer.
	std::memcpy(p->uri,&msg->connect.uri,msg->connect.uri_len);
	
	p->len = msg->connect.buffer_len;

	pg_size = sysconf(_SC_PAGESIZE);

	//Allocate the memory that can be processed.
	ret = posix_memalign(&p->buffer,pg_size,p->len);
	if(!p->buffer){
		throw std::bad_alloc();
	}

	//Register the buffer.
	ret = vio_register(p->buffer,p->len,&p->local);
	if(ret){
		CCI_ERR(ret);
	}

	ret = cci_accept(event,p);
	if(ret){
		CCI_ERR(ret);
	}

	this->mNumPeers++;	

}
//
//=======================================================================================
//
void cServer::HandleAccept(cci_event_t* event)
{
	peer_t *p = (peer_t*)event->accept.context;
	p->conn = event->accept.connection;
	if(!p->conn){
		free_peer(p);
		this->mCommsRunning = false;
		CCI_ERR(EINVAL);
	}
}
//
//=======================================================================================
//
void cServer::HandleWriteReq(cci_event_t* event)
{
	int ret = 0;
	peer_t *p =(peer_t*) event->recv.connection->context;
	vio_msg_t* msg = (vio_msg_t*) event->recv.ptr;
	vio_msg_t reply; //= {.done = {WRITE_DONE,msg->request.cookie}};
	reply.done.type = WRITE_DONE;
	reply.done.cookie = msg->request.cookie;
	vio_req_t* vio = NULL;
	vio = (vio_req_t*)calloc(1,sizeof(*vio));
	if(!vio){
		throw std::bad_alloc();
	}
	vio->peer = p;
	vio->len = msg->request.len;
	memset(&reply,0,sizeof(reply.done));
	reply.done.type = WRITE_DONE;
	reply.done.cookie = msg->request.cookie;
	ret = cci_rma(p->conn,&reply,sizeof(reply.done),p->local,0,
		p->remote,0,vio->len,vio,CCI_FLAG_READ);
	if(ret){
		CCI_ERR(ret);
	}
}
//
//=======================================================================================
//
void cServer::HandleBye(cci_event_t* event)
{
	int ret = 0;
	peer_t* p = (peer_t*)event->recv.connection->context;
	//vio_msg_t msg = {.fini = {FINISHED,0}};
	vio_msg_t msg;
	msg.fini.type = FINISHED;
	msg.fini.pad = 0;
	p->done = 1;
	ret = cci_send(p->conn,&msg,sizeof(msg.fini),VIZ_TX_FINI,0);
	if(ret){
		CCI_ERR(ret);
	}	
	return;

}
//
//=======================================================================================
//
void cServer::HandleReceived(cci_event_t* event)
{
	vio_msg_t* msg = (vio_msg_t*)event->recv.ptr;
	
	switch(msg->generic.type){
		case WRITE_REQ:
			HandleWriteReq(event);
			break;
		case BYE:
			HandleBye(event);
			break;
		default:
			break;
	}	
}
//
//=======================================================================================
//
void cServer::HandleRMA(cci_event_t* event)
{
	//printf ("Arriving to cServer::HandleRMA() \n");
	vio_req_t* vio = (vio_req_t*)event->send.context;
	std::unique_lock<std::mutex> locker(this->mReqLock);
	this->mReqs.push(vio);
	this->mReqCon.notify_one();	
}
//
//=======================================================================================
//
void cServer::HandleFini(cci_event_t* ev,cci_connection_t** c)
{
	//printf ("Arriving to cServer::HandleFini() \n");
	peer_t* p = (peer_t*)ev->send.connection->context;
	*c = p->conn;
	free_peer(p);
	this->mNumPeers-=1;
	//If no one is connected, just quit.
	if(this->mNumPeers == 0){
		this->mCommsRunning = false;
		//Signal the processing thread?	
	}
}
//
//=======================================================================================
//
void cServer::HandleSend(cci_event_t* ev,cci_connection_t** c)
{
	void* ctx = ev->send.context;
	if(ctx == VIZ_TX_FINI){
		this->HandleFini(ev,c);
	}else{
		this->HandleRMA(ev);
	}
}
//
//=======================================================================================
//
void cServer::Comms()
{
	this->mCommsRunning = true;
	int ret = 0;
	cci_event_t* event = NULL;	
	while(this->mCommsRunning){
		cci_connection_t *conn = NULL;

		//Wait for an event to happen.
		ret = cci_get_event(this->mpEndPt,&event);
		if(ret && ret != CCI_EAGAIN){
			CCI_ERR(ret);
			//Add to queue of exceptions?
		}else if(ret){
			continue;
			//Add to queue of exceptions?
		}	

		//Handle the event.
		switch(event->type){
			case CCI_EVENT_CONNECT_REQUEST:
				this->HandleConnectRequest(event);
				break;
			case CCI_EVENT_ACCEPT:
				this->HandleAccept(event);
				break;
			case CCI_EVENT_RECV:
				this->HandleReceived(event);
				break;
			case CCI_EVENT_SEND:
				this->HandleSend(event,&conn);
				break;	
			default:
				break;
		}

		cci_return_event(event);

		if(conn){
			cci_disconnect(conn);
			this->mCommsRunning = false;
		}		
	}
	printf ("cServer::Comms() ends \n");
	return;
}
//
//=======================================================================================
//
void cServer::StopProcess()
{
	this->mProcRunning = false;
}
//
//=======================================================================================
//
bool cServer::is2DArray (const char *name)
{
	if (	!strcmp(name, "gross_nmin_vr_col") 					||
			!strcmp(name, "sminn_to_plant_vr_col") 				||
			!strcmp(name, "actual_immob_vr_col")
		)
		return true;
	return false;
}
//
//=======================================================================================
//
void cServer::ptrTovtkTable (void *in, vtkSmartPointer<vtkTable> out, std::string fieldName_, int dataTypeSize, int rows, int cols)
{
	int 				i,j,i0=0;
	int 				temp;
	int 				idx;
	char*				fieldName	= 0;
	int 				sizeOfCa 	= sizeof("carbonflux_vars%")-1;
	int 				sizeOfN2 	= sizeof("nitrogenflux_vars%")-1;
	int					sizeOfField	= fieldName_.size();


	// removing carbonflux_vars% and nitrogenflux_vars% substrings from the fieldName
	if (!strncmp(fieldName_.c_str(), "carbonflux_vars%", sizeOfCa))
	{
		fieldName = strndup(fieldName_.c_str()+sizeOfCa, sizeOfField);
	}
	else if (!strncmp(fieldName_.c_str(), "nitrogenflux_vars%", sizeOfN2))
	{
		fieldName = strndup(fieldName_.c_str()+sizeOfN2, sizeOfField);
	}
	if (!fieldName)
	{
		std::clog << "There is something wrong with the variable names." << std::endl;
	}

	if (is2DArray(fieldName))
	{
		temp = cols;
		cols = rows;
		rows = temp;

		//setting up vtkTables column names
		for (i=0;i<cols;i++)
		{
			std::stringstream	ssFieldName;
			ssFieldName << fieldName 			<< "_" << i;

			if (dataTypeSize==8) // double
			{
				vtkSmartPointer<vtkDoubleArray> col =
						  vtkSmartPointer<vtkDoubleArray>::New();
				col->SetName(ssFieldName.str().c_str());
				out->AddColumn(col);
			}
			else if (dataTypeSize == 4 ) //float
			{
				vtkSmartPointer<vtkFloatArray> col =
									  vtkSmartPointer<vtkFloatArray>::New();
				col->SetName(ssFieldName.str().c_str());
				out->AddColumn(col);
			}
		}
		out->SetNumberOfRows(rows);


		// copy data from databuffer 2D to vtkTable
		for (i=0; i<rows; i++)
		{
			for (j=0; j<cols; j++)
			{
				idx = j+i*cols;
				if (dataTypeSize == 8) //double
				{
					double *ptr = (double *)in;

					if (isnan( *(ptr+idx) ) != 0 )
						*(ptr+idx) = 0.0;

					out->SetValue( i, j, (*(ptr+idx)) );

					//std::cout << "[" << i << ", " << j << "] = " << *(ptr+idx) << " ";
				}
				else if (dataTypeSize == 4 ) // float
				{
					float *ptr = (float *)in;
					if (isnan( *(ptr+idx) ) != 0 )
						*(ptr+idx) = 0.0f;

					// choosing only the meaningful data (#10, #12 and #13 values)
						out->SetValue( i, j, (*(ptr+idx)) );
					//std::cout << "[" << i << ", " << j << "] = " << *(ptr+idx) << " ";

					//col->InsertNextValue ( vtkVariant ( *(ptr+idx) ) );

				}
			} //for (j=0; j<cols; j++)
		} // for (i=0; i<rows; i++)
	}
	else
	{
		//setting up vtkTables column names
		// According to Dali, only 3 values are meaningful
		// if more are needed, change 3 number
		for (i=0;i<3;i++)
		{
			std::stringstream	ssFieldName;
			ssFieldName << fieldName 			<< "_" << i;

			if (dataTypeSize==8) // double
			{
				vtkSmartPointer<vtkDoubleArray> col =
						  vtkSmartPointer<vtkDoubleArray>::New();
				col->SetName(ssFieldName.str().c_str());
				out->AddColumn(col);
			}
			else if (dataTypeSize == 4 ) //float
			{
				vtkSmartPointer<vtkFloatArray> col =
									  vtkSmartPointer<vtkFloatArray>::New();
				col->SetName(ssFieldName.str().c_str());
				out->AddColumn(col);
			}
		}
		out->SetNumberOfRows(1);

		//filling vtkTables
		// choosing only the meaningful data (#10, #12 and #13 values)
		// if more required add a loop
		if (dataTypeSize == 8) //double
		{
			double *ptr = (double *)in;

			out->SetValue(0,0,*(ptr+9));
			out->SetValue(0,1,*(ptr+11));
			out->SetValue(0,2,*(ptr+12));
		}
		else if (dataTypeSize == 4 ) // float
		{
			float *ptr = (float *)in;

			out->SetValue(0,0,*(ptr+9));
			out->SetValue(0,1,*(ptr+11));
			out->SetValue(0,2,*(ptr+12));
		}
	} //if (is2DArray(fieldName))

	//out->Dump(30);

	//std::cout << std::endl;
	//std::cout << "Number of Rows: " << out->GetNumberOfRows() 		<< std::endl;
	//std::cout << "Number of Cols: " << out->GetNumberOfColumns()	<< std::endl;
	//std::cout << std::endl;
}
//
//=======================================================================================
//
void cServer::insertStepInSimTable ()
{
}
//
//=======================================================================================
//
void cServer::myUnpack ( void *buffer, size_t len, vtkSmartPointer<vtkTable> *_step1D, vtkSmartPointer<vtkTable> *_step2D)
{
	int 		eleSz 		= 0;
	int 		nRow 		= 0;
	int 		nCol 		= 0;
	int			numVar 		= 0;
	int 		NX 			= 1;
	int			NY 			= 1;
	char*		varName 	= 0;
	char*		varType 	= 0;

	size_t 	offset 		= 0;
	size_t 	myLen		= 0;
	size_t	totalLen 	= 0;
	size_t	curPos		= 0;

	std::vector<vtkSmartPointer<vtkTable>> varsTemp;

	curPos 		= sizeof(size_t);
	curPos 	   += sizeof(int); // int flag
	totalLen 	= *((size_t *) buffer);

	//fprintf(stdout, "Lengths - len: %zd, totalLen: %zd\n", len, totalLen);
	//std::cout << " len: " << len << " totalLen: " << totalLen << std::endl;

	while ( curPos < len)
	{
		//std::cout << std::endl;
		varName = (char*)((size_t)buffer + curPos); // getname
	    //fprintf(stdout,"varname: %s\n", varName);
	    //std::cout << numVar++ << ". " << varName;
	    curPos += (strlen(varName) + 1); // advance

	    varType = (char*)((size_t)buffer + curPos); // get var type
	    //fprintf(stdout,"vartpe: %s\n", varType);
	    //std::cout << " type: " << varType;
	    curPos += (strlen(varType) + 1); // advance

	    eleSz = *(int *) ((size_t)buffer + curPos); // get element size
	    //fprintf(stdout,"elem size: %d\n", eleSz);
	    //std::cout << " type size: " << eleSz << std::endl;

	    curPos += sizeof(int); // advance
	    nRow = *(int *) ((size_t)buffer + curPos);
	    NX = nRow; // get num elements in X

	    curPos += sizeof(int); // advance
	    nCol = *(int *) ((size_t)buffer + curPos);
	    NY = nCol; // get num elements in Y
	    //fprintf(stdout,"nrows %d ncols %d\n", nRow, nCol);
	    //std::cout << "[Nx, Ny] = [ " << NX << ", " << NY << " ] \n";

	    curPos += sizeof(int); // advance

	    offset = *(size_t *) ((size_t)buffer + curPos); // get offset

	    curPos += (sizeof(size_t)); // advance

	    myLen = *(size_t *) ((size_t)buffer + curPos); // get buffer length
	    //fprintf(stdout,"mylen: %zd\n", myLen);
	    //std::cout << "Mylen: " << myLen << std::endl;

	    curPos += (sizeof(size_t));	 //advance

	    void *ptr = (void*)((size_t)buffer + offset); // data
	    vtkSmartPointer<vtkTable> variable =  vtkSmartPointer<vtkTable>::New(); // data stored as vtkTable

	    if (isVarOfInterest(varName) )
	    {
	    	ptrTovtkTable (ptr, variable, varName, eleSz, NX, NY);
	    	varsTemp.push_back(variable);
	    }
	    //vars->at(numVar-1)->Dump();

	    //std::cout << timeStep->GetColumnName(0) << std::endl;

	    curPos = offset+myLen;
	}
	// joining variables into one vtkTable
	// first pass get max rows and number of columns
	int i, j;
	int maxRows = 0;
	nCol = 0;

	for (i=0; i<varsTemp.size();i++ )
	{
		//varsTemp.at(i)->Dump(40);
		//std::cout << std::endl;
		if (varsTemp.at (i)->GetNumberOfColumns() <= 3)
		{
			for (j = 0; j<varsTemp.at(i)->GetNumberOfColumns();j++)
				_step1D->GetPointer()->AddColumn(varsTemp.at(i)->GetColumn(j));
		}
		else
		{
			for (j = 0; j<varsTemp.at(i)->GetNumberOfColumns();j++)
				_step2D->GetPointer()->AddColumn(varsTemp.at(i)->GetColumn(j));
		}
	}

	//adding step column counter
	if (!numTimeSteps)
	{
		vtkSmartPointer<vtkFloatArray> col = vtkSmartPointer<vtkFloatArray>::New();
		col->SetName("Step");
		col->InsertNextTuple1(numTimeSteps);
		_step1D->GetPointer()->AddColumn(col);
	}


	// adding  the "Layer" Column
	// adding  the "Step" Column
/*
	vtkSmartPointer<vtkIntArray> layer = vtkSmartPointer<vtkIntArray>::New();
	vtkSmartPointer<vtkIntArray> step = vtkSmartPointer<vtkIntArray>::New();
	layer->SetName("Layer");
	step->SetName("Step");
	for (i=0;i<_step2D->GetPointer()->GetNumberOfRows();i++)
	{
		layer->InsertNextTuple1(i);
		step->InsertNextTuple1(0.0);
	}
	_step2D->GetPointer()->AddColumn(layer);
	_step2D->GetPointer()->AddColumn(step);
*/

	//insertStepInSimTable ();

	///step->SetNumberOfRows(maxRows);
	//_step->GetPointer()->Dump();
	//std::cout<<std::endl;
}
//
//=======================================================================================
//
bool cServer::isVarOfInterest (char* name)
{
	// TODO: specify these in the config file
		if (!strcmp(name, "carbonflux_vars%leaf_curmr_patch") 						||
			!strcmp(name, "carbonflux_vars%froot_curmr_patch") 						||
			!strcmp(name, "carbonflux_vars%livestem_curmr_patch") 					||
			!strcmp(name, "carbonflux_vars%cpool_to_xsmrpool_patch") 				||
			!strcmp(name, "carbonflux_vars%cpool_to_leafc_patch") 					||
			!strcmp(name, "carbonflux_vars%cpool_to_leafc_storage_patch") 			||
			!strcmp(name, "carbonflux_vars%cpool_to_frootc_patch") 					||
			!strcmp(name, "carbonflux_vars%cpool_to_frootc_storage_patch") 			||
			!strcmp(name, "carbonflux_vars%cpool_to_livestemc_patch") 				||
			!strcmp(name, "carbonflux_vars%cpool_to_livestemc_storage_patch")		||
			!strcmp(name, "nitrogenflux_vars%plant_ndemand_patch") 					||
			!strcmp(name, "nitrogenflux_vars%npool_to_leafn_patch") 				||
			!strcmp(name, "nitrogenflux_vars%npool_to_leafn_storage_patch") 		||
			!strcmp(name, "nitrogenflux_vars%npool_to_frootn_patch") 				||
			!strcmp(name, "nitrogenflux_vars%npool_to_frootn_storage_patch") 		||
			!strcmp(name, "nitrogenflux_vars%npool_to_livestemn_patch") 			||
			!strcmp(name, "nitrogenflux_vars%npool_to_livestemn_storage_patch") 	||
			!strcmp(name, "nitrogenflux_vars%gross_nmin_vr_col") 					||
			!strcmp(name, "nitrogenflux_vars%sminn_to_plant_vr_col") 				||
			!strcmp(name, "nitrogenflux_vars%actual_immob_vr_col")
			)
		{
			return true;
		}

	return false;
}
//
//=======================================================================================
//
void cServer::printVarOfInterest (char *name, vtkSmartPointer<vtkTable> table )
{
	// TODO: specify these in the config file
	if (!strcmp(name, "carbonflux_vars%leaf_curmr_patch") 						||
		!strcmp(name, "carbonflux_vars%froot_curmr_patch") 						||
		!strcmp(name, "carbonflux_vars%livestem_curmr_patch") 					||
		!strcmp(name, "carbonflux_vars%cpool_to_xsmrpool_patch") 				||
		!strcmp(name, "carbonflux_vars%cpool_to_leafc_patch") 					||
		!strcmp(name, "carbonflux_vars%cpool_to_leafc_storage_patch") 			||
		!strcmp(name, "carbonflux_vars%cpool_to_frootc_patch") 					||
		!strcmp(name, "carbonflux_vars%cpool_to_frootc_storage_patch") 			||
		!strcmp(name, "carbonflux_vars%cpool_to_livestemc_patch") 				||
		!strcmp(name, "carbonflux_vars%cpool_to_livestemc_storage_patch")		||
		!strcmp(name, "nitrogenflux_vars%plant_ndemand_patch") 					||
		!strcmp(name, "nitrogenflux_vars%npool_to_leafn_patch") 				||
		!strcmp(name, "nitrogenflux_vars%npool_to_leafn_storage_patch") 		||
		!strcmp(name, "nitrogenflux_vars%npool_to_frootn_patch") 				||
		!strcmp(name, "nitrogenflux_vars%npool_to_frootn_storage_patch") 		||
		!strcmp(name, "nitrogenflux_vars%npool_to_livestemn_patch") 			||
		!strcmp(name, "nitrogenflux_vars%npool_to_livestemn_storage_patch") 	||
		!strcmp(name, "nitrogenflux_vars%gross_nmin_vr_col") 					||
		!strcmp(name, "nitrogenflux_vars%sminn_to_plant_vr_col") 				||
		!strcmp(name, "nitrogenflux_vars%actual_immob_vr_col")
		)
		table->Dump(40);

}
//
//=======================================================================================
//
void cServer::myUnpack ( void *buffer, size_t len)
{
	int 		eleSz 		= 0;
	//int 		num 		= 0;
	int 		nRow 		= 0;
	int 		nCol 		= 0;
	//int 		csvcount 	= 0;
	int			numVar 		= 0;
	int 		NX 			= 5;
	int			NY 			= 10;
	//int 		dimids[2];
	//int 		retval, ncid, x_dimid, y_dimid, varid;
	//char 		x_dim_name[1024];
	//char		y_dim_name[1024];
	char*		varName 	= 0;
	char*		varType 	= 0;

	static int	simStep		= 1;

	size_t 	offset 		= 0;
	size_t 	myLen		= 0;
	size_t	totalLen 	= 0;
	size_t	curPos		= 0;

	std::vector<vtkSmartPointer<vtkTable>> varsTemp;



	curPos 		= sizeof(size_t);
	curPos 	   += sizeof(int); // int flag
	totalLen 	= *((size_t *) buffer);

	//fprintf(stdout, "Lengths - len: %zd, totalLen: %zd\n", len, totalLen);
	//std::cout << " len: " << len << " totalLen: " << totalLen << std::endl;

	while ( curPos < len)
	{
		varName = (char*)((size_t)buffer + curPos); // getname
	    //fprintf(stdout,"varname: %s\n", varName);
	    //std::cout << numVar++ << ". " << varName;
	    curPos += (strlen(varName) + 1); // advance

	    varType = (char*)((size_t)buffer + curPos); // get var type
	    //fprintf(stdout,"vartpe: %s\n", varType);
	    //std::cout << " type: " << varType;
	    curPos += (strlen(varType) + 1); // advance

	    eleSz = *(int *) ((size_t)buffer + curPos); // get element size
	    //fprintf(stdout,"elem size: %d\n", eleSz);
	    //std::cout << " type size: " << eleSz << std::endl;

	    curPos += sizeof(int); // advance
	    nRow = *(int *) ((size_t)buffer + curPos);
	    NX = nRow; // get num elements in X

	    curPos += sizeof(int); // advance
	    nCol = *(int *) ((size_t)buffer + curPos);
	    NY = nCol; // get num elements in Y
	    //fprintf(stdout,"nrows %d ncols %d\n", nRow, nCol);
	    //std::cout << "[Nx, Ny] = [ " << NX << ", " << NY << " ] \n";

	    curPos += sizeof(int); // advance

	    offset = *(size_t *) ((size_t)buffer + curPos); // get offset

	    curPos += (sizeof(size_t)); // advance

	    myLen = *(size_t *) ((size_t)buffer + curPos); // get buffer length
	    //fprintf(stdout,"mylen: %zd\n", myLen);
	    //std::cout << "Mylen: " << myLen << std::endl;

	    curPos += (sizeof(size_t));	 //advance

	    void *ptr = (void*)((size_t)buffer + offset); // data
	    vtkSmartPointer<vtkTable> variable =  vtkSmartPointer<vtkTable>::New(); // data stored as vtkTable

	    if (isVarOfInterest(varName) )
	    {
	    	ptrTovtkTable (ptr, variable, varName, eleSz, NX, NY);

	    	varsTemp.push_back(variable);
	    }

	    //printVarOfInterest (varName, variable);


	    //std::cout << timeStep->GetColumnName(0) << std::endl;

	    curPos = offset+myLen;
	}
	// joining variables into one vtkTable
	// first pass get max rows and number of columns
	int i, j, k, l=0;
	int maxRows = 0;
	nCol = 0;

	vtkSmartPointer<vtkVariantArray> temp = vtkSmartPointer<vtkVariantArray>::New();

	for (i=0; i<varsTemp.size();i++ )
	{
		// TODO: watch out harcoded
		if (varsTemp.at (i)->GetNumberOfColumns() <= 3)
		{
			for (j=0; j<varsTemp.at(i)->GetNumberOfColumns();j++)
			{
				temp->InsertNextValue( varsTemp.at(i)->GetValue(0,j));
			}
			continue;
		}
		//varsTemp.at(i)->Dump(30);

		for (j=0; j<varsTemp.at(i)->GetNumberOfColumns();j++,l++)
		{
			simData2DTable->GetColumn(l)->InsertTuples(0, varsTemp.at(i)->GetNumberOfRows(), 0, varsTemp.at(i)->GetColumn(j));
		}

	}

	//simData2DTable->Dump(30);

	// checking if step if ok, then send it to the viz.

	if (temp->GetNumberOfValues() > 0 )
	{
		temp->InsertNextValue( ++numTimeSteps);


		simData1DTable->InsertNextRow(temp);

		simData2D.push_back(simData2DTable);


		//simData1DTable->Dump(30);
		//simData2DTable->Dump(30);


		std::lock_guard<std::mutex> lk(m);
        	std::cout << " cServer::myUnpack signals data ready for Viz\n";
        	timeStepIsReady = true;


        cv.notify_one();

	}
	else
	{
		std::cout << "Warning: There is not variables in step table" << std::endl;
	}

	temp = 0;

	varsTemp.clear();
	std::cout<<std::endl;
}
//
//=======================================================================================
//
void cServer::Process()
{

	this->mProcRunning = true;

	//While the communication part is still running.
	while(this->mProcRunning){

		//printf ("cServer::Process() is running \n");

		//Access the next request.
		std::unique_lock<std::mutex> locker(this->mReqLock);
		this->mReqCon.wait(locker,[&](){return !this->mReqs.empty();});

		//Process each request, one at a time.
		while(!this->mReqs.empty()){
			vio_req_t* vio = mReqs.front();
			this->mReqs.pop();
			peer_t* p = vio->peer;

#ifdef MYUNPACK

			std::cout << "\ntimeStepIsReady before myUnpack " << timeStepIsReady << std::endl;

			myUnpack(p->buffer, vio->len);

			std::unique_lock<std::mutex> lk(m);


			//std::cout << "\ntimeStepIsReady after myunpack " << timeStepIsReady << std::endl;

			cv.wait(lk, []{return !timeStepIsReady;});
			std::cout << "\ntimeStepIsReady after cv.wait " << timeStepIsReady << std::endl;

#else
			//Decompress the buffer.
			//Process vio->len bytes at p->buffer.		
			char* decompressed = new char[p->len];
			uLong uncompressed_size = p->len+1;
			uncompress((Bytef*)decompressed,&uncompressed_size,(Bytef*)p->buffer,
				vio->len);
			printf("Data %s \n",decompressed );
			printf("Decompressed %i \n",p->len);
			delete decompressed;
#endif
		}
	}

	printf ("cServer::Process() ends \n");
	return;
}
//
//=======================================================================================
//
CommProcess cServer::InitServer()
{

	//Is the communication module already running?
	if(this->mCommsRunning){
		//Throw an exception, or log event?
		throw "Already running";
	}

	//Initialize the CCI interface.
	int ret = 0;
	ret = vio_init(&(this->mpEndPt),&(this->mpURI));
	if(ret) CCI_ERR(ret);
	
	//Start listening for incoming messages.
	std::thread* comms = new std::thread(&cServer::Comms,this);

	//Run the thread to process the data.
	std::thread* proc =  new std::thread(&cServer::Process,this);

	return CommProcess(comms,proc);
}
