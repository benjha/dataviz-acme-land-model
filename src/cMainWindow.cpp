

#include <cMainWindow.moc>
#include <qevent.h>
#include <dirent.h>
#include <sys/stat.h>
#include <comm_process.h>
#include <unistd.h>
#include <math.h>
#include <vector>
#include <vtkTransposeTable.h>
#include <mutex>
#include <condition_variable>

#include <vtkVariantArray.h>
#include <vtkAbstractArray.h>
#include <vtkPlotLine.h>
#include <vtkPlotBar.h>
#include <vtkColorSeries.h>
#include <vtkLookupTable.h>
#include <vtkContextMouseEvent.h>

VTK_CREATE (vtkFloatArray, timeAxis);
VTK_CREATE (vtkContextTransform, trans);

// step info. scope global
vtkSmartPointer<vtkTable> step1D = 0;

//vtkTable*					step1DT
vtkSmartPointer<vtkTable> simData1DTable = 0;
vtkSmartPointer<vtkTable> simData2DTable = 0;
std::vector <vtkSmartPointer<vtkTable>> simData2D;

//vtkSmartPointer<vtkTable> step2D = 0;
// global flag to control when time step1D arrives from sim.
bool  timeStepIsReady = false;
unsigned int numTimeSteps = 0;
std::mutex m;
std::condition_variable cv;

//
//=======================================================================================
//
cMainWindow::cMainWindow(int argc, char *argv[])
{
	timeStep		= 1;
	currentTimeStep = 0;
	chartId 		= 0;
	ui 				= new Ui_MainWindow();

	//table 		= vtkSmartPointer<vtkTable>::New();
	serie 		= vtkSmartPointer<vtkTable>::New();
	scratchTable= vtkSmartPointer<vtkTable>::New();
	view 		= vtkSmartPointer<vtkContextView>::New();
	view2 		= vtkSmartPointer<vtkContextView>::New();
	alink 		= vtkSmartPointer<vtkAnnotationLink>::New();


	ui->setupUi(this);
	cciServer = new cServer ();
	commDataProc = cciServer->InitServer();

	if (argc>1)
	{
		loadDatafile(argv[1], cMainWindow::MAPPED_BUFFER);
		//loadMappedDataFiles(argv[1], 100);
	}
	else
	{
		genSomeData ();
	}

	//populateListWidget ();
	populateListWidgetFromSim();
	initCharts();
	initHeatMaps ();

	view->SetInteractor(ui->qvtkWidget->GetInteractor());
	view->GetRenderWindow()->SetMultiSamples(0);
	ui->qvtkWidget->SetRenderWindow(view->GetRenderWindow());
	ui->qvtkWidget->setFocusPolicy(Qt::ClickFocus);


	view2->SetInteractor(ui->qvtkWidget_2->GetInteractor());
	view2->GetRenderWindow()->SetMultiSamples(0);
	ui->qvtkWidget_2->SetRenderWindow(view2->GetRenderWindow());
	ui->qvtkWidget_2->setFocusPolicy(Qt::ClickFocus);

	view->GetInteractor()->Initialize	();
	view2->GetInteractor()->Initialize	();

	//ui->okButton->setEnabled			(false);
	//ui->okButton2DVars->setEnabled		(false);
	//ui->resetButton->setEnabled 		(false);
	//ui->resetButton2DVars->setEnabled	(false);


	//ui->timeStepSlider->setMaximum(0);
	//ui->resetButton->setEnabled(false);


	// Set up action signals and slots
	connect(ui->action_Quit, 		SIGNAL( triggered	( )					), this, SLOT( slotExit			( )					));
	connect(ui->Vars1D,				SIGNAL( itemClicked	(QListWidgetItem *)	), this, SLOT( updateYaxis		(QListWidgetItem *)	));
	connect(ui->Vars2D,				SIGNAL( itemClicked	(QListWidgetItem *)	), this, SLOT( updateYaxis2DVars(QListWidgetItem *)	));
	connect(ui->pftCombo,			SIGNAL( activated	(int )				), this, SLOT( PFTSelection		(int ) 				));
	connect(ui->pftCombo2,			SIGNAL( activated	(int )				), this, SLOT( PFT2DSelection	(int ) 				));

	connect(ui->step0,				SIGNAL( valueChanged(int)				), this, SLOT( setStep0			(int )				));
	connect(ui->step02DVars,		SIGNAL( valueChanged(int)				), this, SLOT( setStep02DVars	(int )				));
	connect(ui->step1,				SIGNAL( valueChanged(int)				), this, SLOT( setStep1			(int )				));
	connect(ui->step12DVars,		SIGNAL( valueChanged(int)				), this, SLOT( setStep12DVars	(int )				));
	connect(ui->timeSeriesVars,		SIGNAL( itemClicked	(QListWidgetItem *)	), this, SLOT( updateTimeSeries	(QListWidgetItem *)	));
	connect(ui->resetButton,		SIGNAL( clicked		(bool )				), this, SLOT( resetPlot		(bool)				));
	connect(ui->resetButton2DVars,	SIGNAL( clicked		(bool )				), this, SLOT( resetPlot		(bool)				));
	connect(ui->okButton,			SIGNAL( clicked		(bool )				), this, SLOT( ok1DVar			(bool)				));
	connect(ui->okButton2DVars,		SIGNAL( clicked		(bool )				), this, SLOT( ok2DVar			(bool)				));


	if (cciServer->URI())
	{
		std::cout 	<< std::endl;
		std::cout	<< "--------------------------------------" << std::endl;
		std::cout 	<< "Server URI: " << cciServer->URI() << std::endl;
		std::cout	<< "--------------------------------------" << std::endl;
		std::cout 	<< std::endl;
		uriInfo 	<< "Server URI: " << cciServer->URI() << std::endl;
		statusBar()->showMessage(QString(uriInfo.str().data()));
	}
	//ui->chartSelected->setText(QString( uriInfo.str().data()));
	//pthread_create (&DataThread, 0, dataLoop, 0);

	qApp->installEventFilter(this);
}
//
//=======================================================================================
//
cMainWindow::~cMainWindow()
{
	cciServer->StopProcess();
	cciServer->StopComms();

//	commDataProc.Comm()->join();
	//commDataProc.Proc()->join();

	delete 	ui;
	delete 	cciServer;

	ui			= 0;
	cciServer	= 0;
	//table 		= 0;
	serie 		= 0;
	view		= 0;
	alink		= 0;

	//pthread_join(DataThread, NULL);

	cciServer = 0;

}
//
//=======================================================================================
//
void cMainWindow::updateXaxis(QListWidgetItem * item)
{

/*
	chartAxisLabel[chartId].xColumn.str( std::string() );
	chartAxisLabel[chartId].xColumn.clear();

	chartAxisLabel[chartId].xColumn << item->text().toStdString() << "_" << chartAxisLabel[chartId].pft;

	charts[chartId]->RemovePlot(0);

	vtkPlot *line;
	if (chartId < NUM_CHARTS / 2 )
	{

		line = charts[chartId]->AddPlot(vtkChart::POINTS);
	}
	else
	{
		line = charts[chartId]->AddPlot(vtkChart::LINE);
	}


	line->SetInputData(simData1DTable, chartAxisLabel[chartId].xColumn.str().c_str(), chartAxisLabel[chartId].yColumn.str().c_str());
	line->SetColor(chartAxisLabel[chartId].color[0], chartAxisLabel[chartId].color[1], chartAxisLabel[chartId].color[2], 255);
	//line->SetWidth(1.0);

	// Set to fixes size, and resize to make it small.
	charts[chartId]->GetAxis(0)->SetTitle(chartAxisLabel[chartId].yColumn.str().c_str());
	charts[chartId]->GetAxis(1)->SetTitle(chartAxisLabel[chartId].xColumn.str().c_str());

	ui->qvtkWidget->update();
	*/
}
//
//=======================================================================================
//
void cMainWindow::updateYaxis(QListWidgetItem * item)
{
	int i = 0;
	std::stringstream selection;



	selection << item->text().toStdString() << "_" << chartAxisLabel[chartId].pft;
	chartAxisLabel[chartId].yColumn.push_back( selection.str() );

	charts[chartId]->ClearPlots();

	VTK_CREATE(vtkColorSeries,colorSerie);
	colorSerie->SetColorScheme(chartAxisLabel[chartId].colorScheme);

	for (i=0; i<chartAxisLabel[chartId].yColumn.size(); i++ )
	{
		 vtkPlot *plot;

		 plot =  charts[chartId]->AddPlot(vtkChart::BAR);

		 plot->SetInputData(simData1DTable, "Step", chartAxisLabel[chartId].yColumn[i].c_str());
		 plot->SetColor(colorSerie->GetColor(i).GetRed(), colorSerie->GetColor(i).GetGreen(), colorSerie->GetColor(i).GetBlue() );

	 }

	//line->SetWidth(1.0);

	// Set to fixes size, and resize to make it small.
	charts[chartId]->GetAxis(0)->SetTitle( "Variable" );
	charts[chartId]->GetAxis(1)->SetTitle( "Step" );
	charts[chartId]->GetAxis(1)->SetRange( chartAxisLabel[chartId].step0, chartAxisLabel[chartId].step1 );
	charts[chartId]->SetShowLegend(true);


	colorSerie = 0;

	ui->qvtkWidget->update();

}
//
//=======================================================================================
//
void cMainWindow::updateYaxis2DVars (QListWidgetItem * item)
{
	std::stringstream selection;

	selection << item->text().toStdString() << "_col_" << chartAxisLabel[chartId].pft;
	chartAxisLabel[chartId].yColumn.push_back( selection.str() );

//	charts[chartId]->ClearPlots();

}
//
//=======================================================================================
//
void cMainWindow::PFTSelection(int idx)
{
	std::cout << "PFT: " << idx << std::endl;
	chartAxisLabel[chartId].pft = idx;
}
//
//=======================================================================================
//
void cMainWindow::PFT2DSelection(int idx)
{
	std::cout << "PFT 2D: " << idx << std::endl;
	chartAxisLabel[chartId].pft = idx;
}
//
//=======================================================================================
//
void cMainWindow::setStep0( int step)
{
	int val = ui->step0->value();
	std::cout << "Step0: " << step << std::endl;
/*
	if ( val >= ui->step1->value())
	{
		ui->step1->setValue( val+1);
	}
*/
	chartAxisLabel[chartId].step0 = val;
	//chartAxisLabel[chartId].step1 = ui->step1->value();

}
//
//=======================================================================================
//
void cMainWindow::setStep02DVars( int step)
{
	int val = ui->step02DVars->value();
	std::cout << "Step0 2D Vars: " << val << std::endl;
/*
	if ( val >= ui->step1->value())
	{
		ui->step1->setValue( val+1);
	}
*/
	chartAxisLabel[chartId].step0 = val;
	//chartAxisLabel[chartId].step1 = ui->step1->value();

}
//
//=======================================================================================
//
void cMainWindow::setStep1( int step)
{
	int val = ui->step1->value ();
	//std::cout << "Step1: " << step << std::endl;
/*
	if (val > numTimeSteps )
	{
		ui->step1->setValue(simData1D.size()-1);
	}
	if ( val<=ui->step0->value() )
	{
		if (val-1 < 0 )
			ui->step0->setValue(0);
		else
			ui->step0->setValue( val-1);
	}
*/
	//chartAxisLabel[chartId].step0 = ui->step0->value();
	chartAxisLabel[chartId].step1 = val;
	ui->okButton->setEnabled(true);

	//ui->plotButton->setEnabled(true);
}
//
//=======================================================================================
//
void cMainWindow::setStep12DVars( int step)
{
	int val = ui->step12DVars->value ();
	std::cout << "Step1 2D Vars: " << val << std::endl;
/*
	if (val > numTimeSteps )
	{
		ui->step1->setValue(simData1D.size()-1);
	}
	if ( val<=ui->step0->value() )
	{
		if (val-1 < 0 )
			ui->step0->setValue(0);
		else
			ui->step0->setValue( val-1);
	}
*/
	//chartAxisLabel[chartId].step0 = ui->step0->value();
	chartAxisLabel[chartId].step1 = val;

	//ui->plotButton->setEnabled(true);
}
//
//=======================================================================================
//
void cMainWindow::resetPlot (bool clicked)
{
	int i;

	charts[chartId]->ClearPlots();

	charts[chartId]->AddPlot(vtkChart::BAR);

	chartAxisLabel[chartId].yColumn.erase(chartAxisLabel[chartId].yColumn.begin(), chartAxisLabel[chartId].yColumn.end());

	//for (i=0;i<chartAxisLabel[chartId].yColumn.size())
}
//
//=======================================================================================
//
void cMainWindow::resetPlot2DVars (bool clicked)
{
	int i;

	charts[chartId]->ClearPlots();

	chartAxisLabel[chartId].yColumn.erase(chartAxisLabel[chartId].yColumn.begin(), chartAxisLabel[chartId].yColumn.end());

	//for (i=0;i<chartAxisLabel[chartId].yColumn.size())
}
//
//=======================================================================================
//
void cMainWindow::ok1DVar( bool clicked )
{
	int i;

	charts[chartId]->GetAxis(1)->SetUnscaledRange( chartAxisLabel[chartId].step0, chartAxisLabel[chartId].step1 );
	charts[chartId]->RecalculateBounds();
	charts[chartId]->GetAxis(vtkAxis::LEFT)->AutoScale();

	ui->qvtkWidget->update();

}
//
//=======================================================================================
//
void cMainWindow::ok2DVar( bool clicked )
{
	int i;

	std:: cout << "PFT: "		<< chartAxisLabel[chartId].pft 		<<
				  " step0: " 	<< chartAxisLabel[chartId].step0 	<<
				  " step1: " 	<< chartAxisLabel[chartId].step1 	<<
				  " Var: " 		<< chartAxisLabel[chartId].yColumn[0] << std::endl;

	VTK_CREATE(vtkTable, qResult);

	query (chartAxisLabel[chartId].yColumn, chartAxisLabel[chartId].step0, chartAxisLabel[chartId].step1, &qResult );

	qResult->Dump(40);

	view2->GetScene()->RemoveItem(trans);


	heatMaps[0]->SetTable(qResult);
	heatMaps[0]->SetCellHeight(8);
	heatMaps[0]->SetCellWidth(8);
	heatMaps[0]->SetOrientation(vtkHeatmapItem::RIGHT_TO_LEFT);

	//heatMaps[0]->GetColumnLabelWidth()

	view2->GetScene()->AddItem(trans);
	ui->qvtkWidget->update();

	ui->qvtkWidget->update();




}
//
//=======================================================================================
//
void cMainWindow::query (std::vector<std::string> colName, int step0, int step1, vtkSmartPointer<vtkTable> *out)
{
	int  i,j;

	//double 			depth[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 };
	vtkStdString 	depthStr[] = {	"0.007100635", "0.027925", "0.06225858", "0.1188651", "0.2121934",
	  							"0.3660658",	"0.6197585", "1.038027", "1.727635", "2.864607", "4.739157",
	  							"7.829766", "12.92532", "21.32647", "35.17762" };

	VTK_CREATE( vtkStringArray, depth );

	depth->SetNumberOfTuples(15);
	depth->SetName("Depth");
	// This is not working:
	//depth->SetArray(depthStr, 15, 1);
	// then doing this:
	depth->SetValue(0, 	"35.17762"		);
	depth->SetValue(1, 	"21.32647"		);
	depth->SetValue(2, 	"12.92532"		);
	depth->SetValue(3, 	"7.829766"		);
	depth->SetValue(4, 	"4.739157"		);
	depth->SetValue(5, 	"2.864607"		);
	depth->SetValue(6, 	"1.727635"		);
	depth->SetValue(7, 	"1.038027"		);
	depth->SetValue(8, 	"0.6197585"		);
	depth->SetValue(9, 	"0.3660658"		);
	depth->SetValue(10, "0.2121934"		);
	depth->SetValue(11, "0.1188651"		);
	depth->SetValue(12, "0.06225858"	);
	depth->SetValue(13, "0.027925"		);
	depth->SetValue(14, "0.007100635"	);

	out->GetPointer()->AddColumn(depth);

	for(i=0; i<simData2D.size(); i++)
	{
		if (i >= step0 && i <= step1)
		{
			VTK_CREATE (vtkTable, temp);
			VTK_CREATE( vtkDoubleArray, data );

			std::stringstream cName;
			cName << " Step " << i << std::endl;
			data->SetName	( cName.str().c_str() );

			temp->AddColumn(simData2D[i]->GetColumnByName(colName[0].c_str()) );

			temp->Dump(30);

			for ( j = simData2D[i]->GetNumberOfRows()-1; j >= 0; j-- )
			{
				data->InsertNextTuple1(temp->GetRow(j)->GetValue(0).ToDouble());
			}

			out->GetPointer()->AddColumn(data);

			//temp = 0;
			//data = 0;

		}

	}
}
//
//=======================================================================================
//
void cMainWindow::updateTimeSeries (QListWidgetItem * item)
{

	int i;

	std::cout << item->text().toStdString() << std::endl;

	VTK_CREATE(vtkColorSeries,colorSerie);
	colorSerie->SetColorScheme(chartAxisLabel[3].colorScheme);


	if (charts[charts.size()-1]->GetNumberOfPlots() > 0)
	{
		charts[charts.size()-1]->ClearPlots();
	}

	// PFT
	// TODO: Watch out hardcoded
	for (i=0;i<3;i++)
	{
		vtkPlot *line;
		line = charts[charts.size()-1]->AddPlot(vtkChart::LINE);

		std::stringstream colName;
		colName << item->text().toStdString() << "_" << i;

		line->SetInputData(simData1DTable, "Step", colName.str().c_str());
		line->SetColor(255, 128, 0, 255);
		line->SetLabel(colName.str().c_str() );
		vtkPlotPoints::SafeDownCast(line)->SetMarkerStyle(vtkPlotPoints::DIAMOND);
		line->SetColor(colorSerie->GetColor(i).GetRed(), colorSerie->GetColor(i).GetGreen(), colorSerie->GetColor(i).GetBlue() );

	}
	charts[charts.size()-1]->SetShowLegend(true);
	charts[charts.size()-1]->GetAxis(0)->SetTitle(item->text().toStdString());
	charts[charts.size()-1]->GetAxis(1)->SetTitle("Time Step");


	colorSerie = 0;

}
//
//=======================================================================================
//
void cMainWindow::updateTimeSeries (int column)
{

}
//
//=======================================================================================
//
void cMainWindow::slotExit()
{
	qApp->exit();
}
//
//=======================================================================================
//
void cMainWindow::loadDatafile(string filename, int type)
{
	switch (type)
	{
		case cMainWindow::CSV:
			parseCSV (filename);
			break;
		case cMainWindow::MAPPED_BUFFER:
			parseBuffer (filename);
			break;
		default:
			std::cout << "There's nothing to open. This will shut down..."  << std::endl;
			break;
	}
}
//
//=======================================================================================
//
void cMainWindow::parseCSV	(string filename)
{
	vtkSmartPointer<vtkDelimitedTextReader>
			reader = vtkSmartPointer<vtkDelimitedTextReader>::New();
	vtkSmartPointer<vtkTable>	_step_ 		= vtkSmartPointer<vtkTable>::New();

	reader->SetFileName(filename.c_str());
	reader->SetHaveHeaders(1);
	reader->DetectNumericColumnsOn();
	reader->SetFieldDelimiterCharacters(",");
	reader->Update();

	_step_ = reader->GetOutput ();
	simData1D.push_back(_step_);
	//_step_->Dump();
	std::cout << _step_->GetNumberOfRows() << std::endl;
}
//
//=======================================================================================
//
void cMainWindow::parseBuffer	(string filename)
{
	int							i;
	int							fileSize 	= 0;
	char*						buffer 		= 0;
	FILE*						fp 			= 0;
	size_t 						len;
	struct stat 				fileStat;
	// creating space for time step
	simData1DTable 		= vtkSmartPointer<vtkTable>::New();
	simData2DTable		= vtkSmartPointer<vtkTable>::New();


	std::cout << "Opening " << filename << std::endl;
	fp = fopen(filename.c_str(), "rb");

	if(!fp)
	{
		std::cerr << "Couldn't open " << filename << std::endl;
		std::cerr << "Shutting down." << std:: endl;
		exit (EXIT_FAILURE);
	}
	if(fstat(fileno(fp),&fileStat) < 0)
	{
		std::cerr << "Unable to read file attributes from " << filename << std::endl;
		std::cerr << "Shutting down." << std:: endl;
		exit (EXIT_FAILURE);
	}

	if (S_ISDIR(fileStat.st_mode))
	{
		std::cerr << "This is a directory. " << filename << std::endl;
		std::cerr << "Shutting down." << std:: endl;
		exit (EXIT_FAILURE);
	}

	fileSize = fileStat.st_size;

	std::clog << "File size (bytes): " << fileSize << std::endl;

	buffer 	= (char *)malloc(sizeof(char) * fileSize);
	len 	= fread(buffer, sizeof(char), fileSize, fp);

	std::clog << "Unpacking..." << std::endl;

	cciServer->myUnpack( buffer, len, &simData1DTable, &simData2DTable);

	simData2D.push_back(simData2DTable);


	simData2D[0]->Dump(30);

	//simData1DTable->Dump(40);
	//simData2DTable->Dump(40);

	fclose (fp);

	//std::cout << _step_->GetNumberOfRows() << std::endl;

}
//
//=======================================================================================
//
void cMainWindow::initCharts ()
{
	int i = 0;
	int j = 1;
	int	k = 0;
	int idx;

	// width and height of the vtk window
	w = ui->qvtkWidget->GetRenderWindow()->GetSize()[0];
	h = ui->qvtkWidget->GetRenderWindow()->GetSize()[1];

	for (int i = 0; i<NUM_CHARTS;i++)
	{
		chartAxisLabel[i].pft 		= 0;
		chartAxisLabel[i].step0 	= 1;
		chartAxisLabel[i].step1 	= 1;
		chartAxisLabel[i].colorScheme = vtkColorSeries::SPECTRUM+i;
	}

	// three charts with points
	for (i=0; i<NUM_CHARTS/2; i++)
	{
		std::stringstream name;
		name << "Plot " << j++;
	 	VTK_CREATE(vtkChartXY, chart);

		// Add multiple line plots, setting the colors etc
		vtkPlot *line = chart->AddPlot(vtkChart::POINTS);

		//arbitrarily choosen:
		//line->SetInputData(simData1DTable, 1, 2);
		line->SetColor(0, 255, 0, 255);
		line->SetWidth(1.0);

		// Set to fixes size, and resize to make it small.
		chart->SetAutoSize(false);
		chart->SetSize(vtkRectf(w/3 * i, h/3.0 * 2, w/3, h/3));
		chart->GetAxis(0)->SetTitle(simData1DTable->GetColumn(2)->GetName());
		//chart->GetAxis(0)->AutoScale();
		chart->GetAxis(1)->SetBehavior(vtkAxis::FIXED);
		chart->GetAxis(1)->SetTitle("Step");
		chart->SetAnnotationLink(alink.GetPointer());
		chart->SetTitle( name.str().data() );
		chart->SetShowLegend(true);
		view->GetScene()->AddItem(chart);

		charts.push_back(chart);
	}

	// three charts with points
	for (i=0; i<NUM_CHARTS/2; i++)
	{
		std::stringstream name;
		name << "Plot " << j++;

		VTK_CREATE(vtkChartXY, chart);

		// Add multiple line plots, setting the colors etc
		vtkPlot *line = chart->AddPlot(vtkChart::POINTS);

		//arbitrarily choosen:
		//line->SetInputData(simData1DTable, 1, 2);
		line->SetColor(0, 255, 0, 255);
		line->SetWidth(1.0);

		// Set to fixes size, and resize to make it small.
		chart->SetAutoSize(false);
		chart->SetSize(vtkRectf(w/3 * i, h/3.0 , w/3, h/3));

		chart->GetAxis(0)->SetTitle(simData1DTable->GetColumn(2)->GetName());
		//chart->GetAxis(0)->AutoScale();
		chart->GetAxis(1)->SetBehavior(vtkAxis::FIXED);
		chart->GetAxis(1)->SetTitle("Step");
		chart->SetAnnotationLink(alink.GetPointer());
		chart->SetTitle( name.str().data() );
		chart->SetShowLegend(true);
		view->GetScene()->AddItem(chart);

		charts.push_back(chart);
	}

	VTK_CREATE (vtkChartXY, chart);


	chart->SetAutoSize(false);
	chart->SetAutoAxes(false);
	chart->SetSize(vtkRectf(0, 0, w, h/3));
	chart->GetAxis(0)->SetTitle("Units");
	chart->GetAxis(1)->SetTitle("Time Step");
	chart->SetAnnotationLink(alink.GetPointer());

	// Time series chart
	{
		std::stringstream name;
		name << "Time Series";

		vtkPlot *line;
		line = chart->AddPlot(vtkChart::POINTS);
		chart->SetTitle( name.str().data() );

		line->SetInputData(simData1DTable, simData1DTable->GetNumberOfColumns()-1, 0);
		line->SetColor(255 - i%255, 128 + i%128, 0, 255);
		line->SetWidth(1.0);
	}

	view->GetScene()->AddItem(chart);
	charts.push_back(chart);



}
//
//=======================================================================================
//
void cMainWindow::initHeatMaps ()
{

	vtkNew<vtkLookupTable> lut;

	lut->SetNumberOfColors(14);
	lut->SetRange(0, 14);
	lut->Build();

	VTK_CREATE(vtkColorSeries,colorSerie);


	VTK_CREATE(vtkDoubleArray, 	values	);
	VTK_CREATE(vtkStringArray, 	labels	);
	VTK_CREATE(vtkAxis, 		axis	);

	double 			depth[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 };
	vtkStdString 	label[] = {	"0.007100635", "0.027925", "0.06225858", "0.1188651", "0.2121934",
								"0.3660658",	"0.6197585", "1.038027", "1.727635", "2.864607", "4.739157",
		  						"7.829766", "12.92532", "21.32647", "35.17762" };


	values->SetArray(depth, 15, 1);
	labels->SetArray(label, 15, 1);
	values->SetName("val");
	labels->SetName("Depth");






	// Dummy Charts just to detect clicks
	VTK_CREATE(vtkChartXY, chart);
	VTK_CREATE(vtkHeatmapItem, heatMap);
	//VTK_CREATE(vtkContextTransform, trans);


	heatMap->SetTable(simData2D[0]);


	trans->SetInteractive(true);
	trans->AddItem(heatMap);
	//trans->Translate(w/3 * i, h/3.0);

	// dummy invisible chart, just to detect clicks
	//chart->SetAutoSize(false);
	//chart->SetSize(vtkRectf(w/3 * i, h/3.0, w/3, h/3));

	heatMap->SetCellHeight(8);
	heatMap->SetCellWidth(8);
			//heatMap->SetOrientation(vtkHeatmapItem::RIGHT_TO_LEFT);
			//heatMap->SetOrientation(vtkHeatmapItem::_TO_LEFT);


	view2->GetScene()->AddItem(trans);

	heatMaps.push_back(heatMap);

}
//
//=======================================================================================
//
void cMainWindow::updateCharts ( int step)
{
	/*
	int i = 0;
	currentTimeStep = step;

	//vtkSmartPointer<vtkTable> table = simData.at(simData.size()-1);
	vtkSmartPointer<vtkTable> table = simData1D.at(currentTimeStep);
	//std::cout << "Num steps: " << simData.size() << " visualizing this step1D " << std::endl;
	//table->Dump();

	//int idx_x = ui->xAxisVars->currentRow() > 0 ? ui->xAxisVars->currentRow() : 0;
	//int idx_y = ui->yAxisVars->currentRow() > 0 ? ui->yAxisVars->currentRow() : 0;

	//std::cout << "Item X: " << ui->text().toStdString() << " idx: " << ui->xAxisVars->currentRow() << std::endl;
	//std::cout << "Item Y: " << ui->text().toStdString() << " idy: " << ui->yAxisVars->currentRow() << std::endl;

	for (i=0; i<charts.size()-1; i++)
	{
		charts[i]->RemovePlot(0);
		vtkPlot *line;
		if (i<3)
		{
			line = charts[i]->AddPlot(vtkChart::POINTS);
		}
		else
		{
			line = charts[i]->AddPlot(vtkChart::LINE);
		}

		line->SetInputData(table, chartAxisLabel[i].xColumn, chartAxisLabel[i].yColumn);
		line->SetColor(0, 255, 0, 255);
		line->SetWidth(1.0);

		// Set to fixes size, and resize to make it small.
		charts[i]->GetAxis(0)->SetTitle(table->GetColumn(chartAxisLabel[i].yColumn)->GetName());
		charts[i]->GetAxis(1)->SetTitle(table->GetColumn(chartAxisLabel[i].xColumn)->GetName());

	}
	ui->qvtkWidget->update();
	*/
}
//
//=======================================================================================
//
void cMainWindow::updateCharts ( )
{
	/*
	int i = 0;

	ui->timeStepSlider->setMaximum(simData1D.size()-1);

	//vtkSmartPointer<vtkTable> table = simData.at(simData.size()-1);
	vtkSmartPointer<vtkTable> table = simData1D.at(currentTimeStep);
	//std::cout << "Current Time Step: " << currentTimeStep << std::endl;
	//table->Dump();

	//int idx_x = ui->xAxisVars->currentRow() > 0 ? ui->xAxisVars->currentRow() : 0;
	//int idx_y = ui->yAxisVars->currentRow() > 0 ? ui->yAxisVars->currentRow() : 0;

	//std::cout << "Item X: " << ui->text().toStdString() << " idx: " << ui->xAxisVars->currentRow() << std::endl;
	//std::cout << "Item Y: " << ui->text().toStdString() << " idy: " << ui->yAxisVars->currentRow() << std::endl;

	for (i=0; i<charts.size()-1; i++)
	{
		charts[i]->RemovePlot(0);
		vtkPlot *line;
		if (i<3)
		{
			line = charts[i]->AddPlot(vtkChart::POINTS);
		}
		else
		{
			line = charts[i]->AddPlot(vtkChart::LINE);
		}

			line->SetInputData(table, chartAxisLabel[i].xColumn, chartAxisLabel[i].yColumn);
			line->SetColor(0, 255, 0, 255);
			line->SetWidth(1.0);

		// Set to fixes size, and resize to make it small.
		charts[i]->GetAxis(0)->SetTitle(table->GetColumn(chartAxisLabel[i].yColumn)->GetName());
		charts[i]->GetAxis(1)->SetTitle(table->GetColumn(chartAxisLabel[i].xColumn)->GetName());

	}

	ui->qvtkWidget->update();
	*/
}
//
//=======================================================================================
//
void cMainWindow::genSomeData ()
{/*
	VTK_CREATE (vtkFloatArray, arrX);
	VTK_CREATE (vtkFloatArray, arrC);
	VTK_CREATE (vtkFloatArray, arrS);
	VTK_CREATE (vtkFloatArray, arrS2);

	arrX->SetName("Field 1");
	table->AddColumn(arrX);


	arrC->SetName("Field 2");
	table->AddColumn(arrC);

	arrS->SetName("Field 3");
	table->AddColumn(arrS);

	arrS2->SetName("Field 4");
	table->AddColumn(arrS2);

	// Test charting with a few more points...
	table->SetNumberOfRows(10);

	for (int i = 0; i < 10; ++i)
	{
	  table->SetValue(i, 0, 0 * i);
	  table->SetValue(i, 1, 1 * i);
	  table->SetValue(i, 2, 2 * i);
	  table->SetValue(i, 3, 3 * i);
	}
	*/
}
//
//=======================================================================================
//

void cMainWindow::populateListWidget ()
{/*
	for (vtkIdType i = 0; i < table->GetNumberOfColumns(); ++i)
	{
#ifdef VERBOSE
		if (vtkStringArray::SafeDownCast(table->GetColumn(i)))
		{
			std::cout << i << " is "
					<< "StringArray named ";
		}
		else if (vtkDoubleArray::SafeDownCast(table->GetColumn(i)))
		{
			std::cout << i << " is "
					<< "DoubleArray named ";
		}
		else if (vtkFloatArray::SafeDownCast(table->GetColumn(i)))
		{
			std::cout << i << " is "
					<< "DoubleArray named ";
		}
		else if (vtkIntArray::SafeDownCast(table->GetColumn(i)))
		{
			std::cout << i << " is "
					<< "IntArray named ";
		}
		else
		{
			std::cout << i << " is "
					<< "Unknown type named ";
		}
#endif
		//std::cout << "\"" << table->GetColumn(i)->GetName() << "\"" << std::endl;

		ui->xAxisVars->addItem(table->GetColumn(i)->GetName());
		ui->yAxisVars->addItem(table->GetColumn(i)->GetName());
	}
*/
}

//
//=======================================================================================
//
void cMainWindow::populateListWidgetFromSim ()
{
	int i;

	std::stringstream sString;
	//sString;

	if (simData1DTable->GetNumberOfColumns() == 0)
	{
		std::cout << "There is not data to visualize." << std::endl;
		exit (EXIT_FAILURE);
	}

	std::cout << "Variables in Simulation: " << simData1DTable->GetNumberOfColumns()/3 << std::endl;

	char aux[128];
	char fieldName[128];
	char colName[128];
	for (i=0; i<simData1DTable->GetNumberOfColumns()-1; i++)
	{

		strcpy (colName, simData1DTable->GetColumn(i)->GetName());
		strncpy (fieldName, colName, strlen(colName) - 2);
		fieldName[strlen(colName) - 2] = '\0';
		if ( strcmp(fieldName, aux ) )
		{
			ui->Vars1D->addItem(fieldName);
			//ui->Vars1Dx->addItem(fieldName);
			ui->timeSeriesVars->addItem(fieldName);
		}
		strcpy ((char*)aux, fieldName);

	}
}
//
//=======================================================================================
//
bool cMainWindow::eventFilter(QObject *obj, QEvent *event)
{
	int i;
	if (event->type() == QEvent::MouseMove)
	{
		//QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
		//statusBar()->showMessage(QString("Mouse move (%1,%2)").arg(mouseEvent->pos().x()).arg(mouseEvent->pos().y()));
	}

	if (event->type() == QEvent::MouseButtonPress)
	{
		QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
		if (mouseEvent->button() == Qt::LeftButton)
		{
			if (obj == ui->qvtkWidget)
			{
				std::stringstream name;
				chartId = getChartClicked (mouseEvent->pos().x(), mouseEvent->pos().y());
				std::cout << "chartId: " << chartId << std::endl;
				name << "Plot " << chartId+1 << " Selected ";
				//ui->chartSelected->setText(name.str().data());
				statusBar()->showMessage(name.str().data());
/*
				if (chartId >= 0 && chartId <=2)
				{
					ui->okButton->setEnabled(true);
					ui->okButton2DVars->setEnabled(false);
				}
				if (chartId >= 3 && chartId <=5)
				{
					ui->okButton->setEnabled(false);
					ui->okButton2DVars->setEnabled(true);

				}
				*/
			}
		}
	}

	if (timeStepIsReady)
	{

		std::lock_guard<std::mutex> lk(m);
					timeStepIsReady = false;

		std::cout << "cMainWindow::eventFilter signals needs more data for viz\n";

		std::cout << "simData1D.push_back(step1D); in eventFilter" << std::endl;

		simData1DTable->Modified();
/*

		timeStep++;
		simData1D.push_back(step1D);
		step1D = 0;

		simData1DTable->Dump(30);

		std::cout << "eventFilter(QObject *obj, QEvent *event) Num steps " << simData1D.size() << std::endl;

		//updateCharts ( );

		//updateTimeSeries(8);

		//charts[6]->GetAxis(0)->SetTitle(lastTimeSeriesItem);

		//serie->Dump ();
		//std::cout << "cMainWindow::eventFilter(QObject *obj, QEvent *event) Num Columns in serie: " << serie->GetNumberOfColumns() << std::endl;
		//std::cout << "cMainWindow::eventFilter(QObject *obj, QEvent *event) Num plots: " << charts[6]->GetNumberOfPlots() << std::endl;


		charts[6]->ClearPlots();

		for (i=0;i<serie->GetNumberOfColumns()-1;i++)
		{

			vtkPlot *line;
			if (simData.size()>1)
			{
				line = charts[6]->AddPlot(vtkChart::LINE);
			}
			else
			{
				line = charts[6]->AddPlot(vtkChart::POINTS);
			}
			line->SetInputData(serie,serie->GetNumberOfColumns()-1, i);
			line->SetColor(255 - i%128, 128 + i%128, 0, 255);
			line->SetWidth(1.0);

		}

		// Set to fixes size, and resize to make it small.
		charts[6]->GetAxis(0)->SetTitle("Units");
		charts[6]->GetAxis(1)->SetTitle("Time Step");

*/
		//lk.unlock();
		cv.notify_one();

	//


  	}

	ui->qvtkWidget->update();
	return false;
}
//
//=======================================================================================
//
int cMainWindow::getChartClicked (int mouseX, int mouseY)
{

	unsigned int 	i;
	double 			x, y, w, h;

	vtkRectf chartSize;

	// ommit time series chart
	for (i=0;i<charts.size()-1;i++)
	{
		chartSize = charts[i]->GetSize();
		std::cout << chartSize.GetX() << " " << chartSize.GetY() << std::endl;

		x = chartSize.GetX();
		y = chartSize.GetY();
		w = chartSize.GetWidth();
		h = chartSize.GetHeight();

		if (mouseX > x && mouseX < (x + w) && (this->h-mouseY) > y && (this->h-mouseY) < y+h)
		{
			return i;
		}
	}

}
//
//=======================================================================================
//
