#ifndef COMM_PROCESS_H
#define COMM_PROCESS_H

#include <thread>



class CommProcess{

public:

	CommProcess()
	{
		mComm = 0;
		mProc = 0;
	}

	CommProcess(std::thread* mcomm,std::thread* mproc)
	{
		mComm = mcomm;
		mProc= mproc;
	}
	~CommProcess() {};

	std::thread* Comm() {return mComm; };
	std::thread* Proc() {return mProc; 	};

private:

	std::thread* mComm;
	std::thread* mProc;
	
};


#endif
