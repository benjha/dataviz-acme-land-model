/********************************************************************************
** Form generated from reading UI file 'gui.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GUI_H
#define UI_GUI_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QFrame>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QListWidget>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpinBox>
#include <QtGui/QStatusBar>
#include <QtGui/QWidget>
#include "QVTKWidget.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *action_Quit;
    QWidget *centralwidget;
    QGroupBox *groupBox;
    QLabel *label;
    QComboBox *pftCombo;
    QListWidget *Vars2D;
    QSpinBox *step0;
    QSpinBox *step1;
    QLabel *label_2;
    QLabel *label_4;
    QPushButton *resetButton;
    QLabel *label_6;
    QListWidget *Vars1D;
    QLabel *label_5;
    QLabel *label_7;
    QComboBox *pftCombo2;
    QFrame *line_2;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QSpinBox *step12DVars;
    QSpinBox *step02DVars;
    QPushButton *resetButton2DVars;
    QPushButton *okButton;
    QPushButton *okButton2DVars;
    QLabel *label_3;
    QListWidget *timeSeriesVars;
    QVTKWidget *qvtkWidget;
    QVTKWidget *qvtkWidget_2;
    QMenuBar *menubar;
    QMenu *menu_File;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1900, 1008);
        action_Quit = new QAction(MainWindow);
        action_Quit->setObjectName(QString::fromUtf8("action_Quit"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        groupBox = new QGroupBox(centralwidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setEnabled(true);
        groupBox->setGeometry(QRect(10, 10, 251, 951));
        QFont font;
        font.setPointSize(10);
        font.setBold(true);
        font.setWeight(75);
        groupBox->setFont(font);
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(20, 20, 31, 16));
        pftCombo = new QComboBox(groupBox);
        pftCombo->setObjectName(QString::fromUtf8("pftCombo"));
        pftCombo->setGeometry(QRect(90, 20, 75, 24));
        pftCombo->setMinimumContentsLength(0);
        pftCombo->setModelColumn(0);
        Vars2D = new QListWidget(groupBox);
        new QListWidgetItem(Vars2D);
        new QListWidgetItem(Vars2D);
        new QListWidgetItem(Vars2D);
        Vars2D->setObjectName(QString::fromUtf8("Vars2D"));
        Vars2D->setGeometry(QRect(30, 770, 201, 51));
        step0 = new QSpinBox(groupBox);
        step0->setObjectName(QString::fromUtf8("step0"));
        step0->setGeometry(QRect(150, 330, 81, 24));
        step0->setMinimum(0);
        step0->setMaximum(10000);
        step1 = new QSpinBox(groupBox);
        step1->setObjectName(QString::fromUtf8("step1"));
        step1->setGeometry(QRect(150, 360, 81, 24));
        step1->setMinimum(1);
        step1->setMaximum(10000);
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(100, 330, 41, 16));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(110, 360, 21, 16));
        resetButton = new QPushButton(groupBox);
        resetButton->setObjectName(QString::fromUtf8("resetButton"));
        resetButton->setEnabled(true);
        resetButton->setGeometry(QRect(140, 390, 80, 25));
        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(90, 50, 52, 14));
        Vars1D = new QListWidget(groupBox);
        Vars1D->setObjectName(QString::fromUtf8("Vars1D"));
        Vars1D->setGeometry(QRect(10, 70, 231, 251));
        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(20, 350, 81, 16));
        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(30, 730, 31, 16));
        pftCombo2 = new QComboBox(groupBox);
        pftCombo2->setObjectName(QString::fromUtf8("pftCombo2"));
        pftCombo2->setGeometry(QRect(90, 730, 75, 24));
        pftCombo2->setMinimumContentsLength(0);
        pftCombo2->setModelColumn(0);
        line_2 = new QFrame(groupBox);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setGeometry(QRect(10, 690, 231, 16));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);
        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(20, 850, 81, 16));
        label_9 = new QLabel(groupBox);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(110, 860, 21, 16));
        label_10 = new QLabel(groupBox);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setGeometry(QRect(100, 830, 41, 16));
        step12DVars = new QSpinBox(groupBox);
        step12DVars->setObjectName(QString::fromUtf8("step12DVars"));
        step12DVars->setGeometry(QRect(150, 860, 81, 24));
        step12DVars->setMinimum(2);
        step12DVars->setMaximum(10000);
        step02DVars = new QSpinBox(groupBox);
        step02DVars->setObjectName(QString::fromUtf8("step02DVars"));
        step02DVars->setGeometry(QRect(150, 830, 81, 24));
        step02DVars->setMinimum(1);
        step02DVars->setMaximum(10000);
        resetButton2DVars = new QPushButton(groupBox);
        resetButton2DVars->setObjectName(QString::fromUtf8("resetButton2DVars"));
        resetButton2DVars->setEnabled(true);
        resetButton2DVars->setGeometry(QRect(150, 900, 80, 25));
        okButton = new QPushButton(groupBox);
        okButton->setObjectName(QString::fromUtf8("okButton"));
        okButton->setGeometry(QRect(30, 390, 80, 25));
        okButton2DVars = new QPushButton(groupBox);
        okButton2DVars->setObjectName(QString::fromUtf8("okButton2DVars"));
        okButton2DVars->setGeometry(QRect(40, 900, 80, 25));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(80, 430, 91, 16));
        timeSeriesVars = new QListWidget(groupBox);
        timeSeriesVars->setObjectName(QString::fromUtf8("timeSeriesVars"));
        timeSeriesVars->setGeometry(QRect(10, 450, 231, 231));
        label->raise();
        pftCombo->raise();
        step0->raise();
        step1->raise();
        label_2->raise();
        label_4->raise();
        resetButton->raise();
        Vars2D->raise();
        label_6->raise();
        Vars1D->raise();
        label_5->raise();
        label_7->raise();
        pftCombo2->raise();
        line_2->raise();
        label_8->raise();
        label_9->raise();
        label_10->raise();
        step12DVars->raise();
        step02DVars->raise();
        resetButton2DVars->raise();
        okButton->raise();
        okButton2DVars->raise();
        label_3->raise();
        timeSeriesVars->raise();
        qvtkWidget = new QVTKWidget(centralwidget);
        qvtkWidget->setObjectName(QString::fromUtf8("qvtkWidget"));
        qvtkWidget->setGeometry(QRect(270, 20, 1621, 681));
        qvtkWidget_2 = new QVTKWidget(centralwidget);
        qvtkWidget_2->setObjectName(QString::fromUtf8("qvtkWidget_2"));
        qvtkWidget_2->setGeometry(QRect(270, 710, 1621, 251));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1900, 22));
        menu_File = new QMenu(menubar);
        menu_File->setObjectName(QString::fromUtf8("menu_File"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(menu_File->menuAction());
        menu_File->addAction(action_Quit);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        action_Quit->setText(QApplication::translate("MainWindow", "&Quit", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("MainWindow", "Plot Tools", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindow", "PFT", 0, QApplication::UnicodeUTF8));
        pftCombo->clear();
        pftCombo->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "9", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "11", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "12", 0, QApplication::UnicodeUTF8)
        );

        const bool __sortingEnabled = Vars2D->isSortingEnabled();
        Vars2D->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem = Vars2D->item(0);
        ___qlistwidgetitem->setText(QApplication::translate("MainWindow", "gross_nmin_vr", 0, QApplication::UnicodeUTF8));
        QListWidgetItem *___qlistwidgetitem1 = Vars2D->item(1);
        ___qlistwidgetitem1->setText(QApplication::translate("MainWindow", "sminn_to_plant_vr", 0, QApplication::UnicodeUTF8));
        QListWidgetItem *___qlistwidgetitem2 = Vars2D->item(2);
        ___qlistwidgetitem2->setText(QApplication::translate("MainWindow", "actual_immob_vr", 0, QApplication::UnicodeUTF8));
        Vars2D->setSortingEnabled(__sortingEnabled);

        label_2->setText(QApplication::translate("MainWindow", "from", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("MainWindow", "to", 0, QApplication::UnicodeUTF8));
        resetButton->setText(QApplication::translate("MainWindow", "&Clear", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("MainWindow", "Y-Axis", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("MainWindow", "Time Step", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("MainWindow", "PFT", 0, QApplication::UnicodeUTF8));
        pftCombo2->clear();
        pftCombo2->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "1", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "2", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "3", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "4", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "5", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "6", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "7", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "8", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "9", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "10", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "11", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "12", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "13", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "14", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "15", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "16", 0, QApplication::UnicodeUTF8)
        );
        label_8->setText(QApplication::translate("MainWindow", "Time Step", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("MainWindow", "to", 0, QApplication::UnicodeUTF8));
        label_10->setText(QApplication::translate("MainWindow", "from", 0, QApplication::UnicodeUTF8));
        resetButton2DVars->setText(QApplication::translate("MainWindow", "&Clear", 0, QApplication::UnicodeUTF8));
        okButton->setText(QApplication::translate("MainWindow", "&OK", 0, QApplication::UnicodeUTF8));
        okButton2DVars->setText(QApplication::translate("MainWindow", "&OK", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("MainWindow", "Time Series", 0, QApplication::UnicodeUTF8));
        menu_File->setTitle(QApplication::translate("MainWindow", "&File", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GUI_H
