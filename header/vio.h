#ifndef VIO_H
#define VIO_H

#include <stdint.h>
#include <stdlib.h> 
#include "cci.h"

//Written by Scott.

/* C = client - Sim process
 * S = server - Viz process
 * C->S = client to server message
 * C<-S = server to client message
 */



typedef enum vio_msg_type {
	CONNECT = 0,	/* C->S: Connection request */
	WRITE_REQ,	/* C->S: Write request */
	WRITE_DONE,	/* C<-S: Write is done - client can reuse buffer */
	BYE,		/* C->S: Client is done */
	FINISHED	/* C<-S: Server acks client's BYE */
} vio_msg_type_t;

typedef union vio_msg {
	struct vio_generic {
		uint32_t type		: 4;	/* Msg type */
		uint32_t pad		:28;	/* Pad */
	} generic;

	struct vio_msg_connect {
		uint32_t type		: 4;	/* CONNECT */
		uint32_t uri_len	:28;	/* Length of client's URI */

		uint32_t buffer_len;		/* Length of buffer */
		cci_rma_handle_t handle;	/* CCI RMA handle */

		char uri[1];			/* Start of client's URI */
	} connect;

	struct vio_msg_write_request {
		uint32_t type		: 4;	/* WRITE_REQ */
		uint32_t cookie		:28;	/* Opaque ID */

		uint32_t len;			/* Length of write */
	} request;

	struct vio_msg_write_done {
		uint32_t type		: 4;	/* WRITE_DONE */
		uint32_t cookie		:28;	/* Opaque ID */
	} done;

	struct vio_msg_write_bye {
		uint32_t type		: 4;	/* BYE */
		uint32_t pad		:28;
	} bye;

	struct vio_msg_write_fini {
		uint32_t type		: 4;	/* FINISHED */
		uint32_t pad		:28;
	} fini;

	vio_msg ()
	{

	}
} vio_msg_t;

/*!
  Init the vio library

  The function will initialize CCI and open an endpoint.

  \param[inout] endpointp	Pointer to cci_endpoint_t pointer.
                                On success, endpointp will point to the CCI endpoint.
  \param[inout] uri		Pointer to char *. On success, uri will point to
                                the CCI endpoint's URI.

  The Sim process should pass NULL for both endpointp and urip.

  \return 0			Success
  \return ENOMEM		Unable to allocate internal structures
  \return EIO			Error within CCI

  Each Sim and Viz process must call once.
*/
int vio_init(cci_endpoint_t **endpointp, char **urip);


/*!
  Register the buffer

  The caller passes in the buffer and its length to be registered with CCI.

  \param[in] buffer		Pointer to buffer to register
  \param[in] len		Length of the buffer
  \param[inout] handle		Address of a CCI RMA handle pointer

  \return 0			Success
  \return EINVAL		Buffer is NULL or len is zero
  \return ENOMEM		Unable to allocate internal structures
  \return EIO			Error within CCI

  Each Sim process must call once on the buffer to be transferred. The Sim
  process should pass NULL to handle.
*/
int vio_register(void *buffer, uint32_t len, cci_rma_handle_t **handle);


/*!
  Establish connection to the viz process

  \param[in] server_uri		The viz process' CCI URI

  \return 0			Success
  \return EINVAL		server_uri is NULL
  \return EINVAL		server_uri is invalid
  \return ENOMEM		Unable to allocate memory
  \return EHOSTUNREACH		Unable to contact viz process
  \return EIO			Error within CCI

  Each Sim process must call once.
*/
int vio_connect(char *server_uri);


/*!
  Shutdown a connection to the viz process

  This call will:

  1) Marshal and send a BYE msg
  2) Wait for FINISHED response
  3) Disconnect

  \return 0			Success
  \return EIO			Error within CCI

  Each Sim process must call once before vio_finalize().
*/
int vio_disconnect(void);


/*!
  Write the buffer

  The semantics are similar to write(). The call is blocking until the write
  completes. Unlike write(), vio_write() either succeeds completely or fails
  completely (i.e. no partial writes).

  This call will marshal and send a WRITE_REQ message to the viz process. When
  the viz process has a RMA buffer available, it will RMA Read this buffer.
  When the RMA Read completes, it will send a WRITE_DONE message back to the
  caller.  The caller will wait in cci_get_event() until then.

  \param[in] len	Length to write

  \return 0		Success, data written to viz process
  \return EINVAL	len is larger than buffer_len
  \return ENOTCONN	Not connected to viz process
  \return EIO		Error within CCI

  When vio_write() returns, the data has been sent to the viz process and the
  buffer may be reused.

  Only the Sim process may call this function.
*/
int vio_write(uint32_t len);


/*!
  Release all resources

  This call will:

  1) Deregister the buffer
  2) Close the CCI endpoint
  3) Finalize CCI

  Each Sim and Viz process must call once.
*/
int vio_finalize(void);



#endif /* VIO_H */
